import { Button } from "flowbite-react";
import "node_modules/video-react/dist/video-react.css"
import React from "react";
import { Player, ControlBar } from "video-react";



const sources = {  sintelTrailer: 'http://media.w3.org/2010/05/sintel/trailer.mp4',
bunnyTrailer: 'http://media.w3.org/2010/05/bunny/trailer.mp4',
test: 'http://media.w3.org/2010/05/video/movie_300.webm'
}

class Video extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            source: sources.sintelTrailer
        };

        this.play = this.play.bind(this);
        this.pause = this.pause.bind(this);
        this.load = this.load.bind(this);
        this.changeCurrentTime = this.changeCurrentTime.bind(this);
        this.seek = this.seek.bind(this);
        this.changePlaybackRateRate = this.changePlaybackRateRate.bind(this);
        this.changeVolume = this.changeVolume.bind(this);
        this.setMuted = this.setMuted.bind(this);

    }

    componentDidMount() {
        this.player.subscribeToStateChange(this.handleStateChange.bind(this));
    }

    setMuted(muted) {
        return () => {
            this.player.muted = muted;
        };
    }

    handleStateChange(state) {
        this.setState({ player: state });
    }

    play() {

        this.player.play();

    }
    pause() {
        this.player.pause();
    }

    load() { this.player.load(); }

    changeCurrentTime(seconds) {
        return () => {
            const { player } = this.player.getState();
            this.player.seek(player.currentTime + seconds);
        };

    }

    seek(seconds) {
        return () => {
            this.player.seek(seconds);
        };
    }

    changePlaybackRateRate(steps) {
        return () => {
            const { player } = this.player.getState();
            this.player.playbackRate = player.playbackRate + steps;
        };
    }

    changeVolume(steps) {
        return () => {
            const { player } = this.player.getState();
            this.player.volume = player.volume + steps;
        };
    }

    changeSource(name) {
        return () => {
            this.setState({
                source: sources[name]
            });
            this.player.load();
        };
    }

    render(){
        return (
            <div>

                <h2 className='m-2'>
                    reference : <a href='https://github.com/video-react/video-react'>https://github.com/video-react/video-react</a>
                </h2>
                <Player
                    playsInline
                    ref={
                        player => {
                            this.player = player;
                        }
                    }

                    autoPlay>
                    <source src={this.state.source} />
                    <ControlBar autoHide={false} />

                </Player>

                <div className="py-3 grid grid-cols-3 gap-2 flex">

                    <Button onClick={this.play} className='bg-rose-500'>
                        Play
                    </Button>

                    <Button onClick={this.pause} className='bg-rose-500'>
                        Pause
                    </Button>

                    <Button onClick={this.load} className='bg-rose-500'>
                        Load
                    </Button>

                </div>

                <hr className='mb-3'></hr>

                <div className="pb-3 grid grid-cols-4 gap-2 flex">

                    <Button onClick={this.changePlaybackRateRate(1)} className="mr-3">
                        playbackRate++
                    </Button>

                    <Button onClick={this.changePlaybackRateRate(-1)} className="mr-3"> playbackRate-
                    </Button>

                    <Button onClick={this.changePlaybackRateRate(0.1)} className="mr-3">
                        playbackRate+=0.1
                    </Button>

                    <Button onClick={this.changePlaybackRateRate(-0.1)} className="mr-3">
                        playbackRate-=0.1
                    </Button>

                </div>

                <div className="pb-3 grid grid-cols-4 gap-2 flex">
                    <Button onClick={this.changeVolume(0.1)} className="mr-3">
                        volume+=0.1
                    </Button>

                    <Button onClick={this.changeVolume(-0.1)} className="mr-3">
                        volume-=0.1
                    </Button>

                    <Button onClick={this.setMuted(true)} className="mr-3">
                        muted=true
                    </Button>

                    <Button onClick={this.setMuted(false)} className="mr-3">
                        muted=false
                    </Button>

                </div>

                <hr className='mb-3'></hr>
                <h1 className='mb-3'>Playlist Video</h1>

                <div className="pb-3 grid grid-cols-3 gap-2 flex">

                    <Button onClick={this.changeSource('sintelTrailer')} className="mr-3 bg-indigo-600">
                        Sintel teaser
                    </Button>

                    <Button onClick={this.changeSource('bunnyTrailer')} className="mr-3 bg-indigo-600">
                        Bunny trailer
                    </Button>

                    <Button onClick={this.changeSource('test')} className="mr-3 bg-indigo-600">
                        Test movie
                    </Button>
                </div>
                <hr></hr>

                <div>State</div>

                <pre className='language-json'>
                    <code className='language-html'>
                        {JSON.stringify(this.state.player, null, 2)}
                    </code>
                </pre>
            </div >
        )
    }
}
export default Video;