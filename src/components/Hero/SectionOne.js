import Video from "../Media/Video";
import Pdf2 from "../Pdf/Pdf2";
import PdfRender from "../Pdf/PdfRender";

export default function SectionOne() {

    return (

        <section className="bg-white dark:bg-gray-900">

            <div className="py-8 px-4 mx-auto max-w-screen-xl text-center lg:py-16 lg:px-12">

                <h1 className="mb-4 text-4xl font-extrabold tracking-tight leading-none text-gray-900 md:text-5xl lg:text-6xl dark:text-white">

                    Selamat datang di website kami!

                </h1>

                <p className="mb-8 text-lg font-normal text-gray-500 lg:text-xl sm:px-16 xl:px-48 dark:text-gray-400">

                    lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.

                </p>

                <div className="mx-auto text-center ">

                    <span className="font-semibold text-gray-400 uppercase">OUR CONTENTS</span>

                    <div className="grid grid-cols-2 gap-4 mt-10 flex justify-center">

                        <div className="max-w bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">

                            <div className="p-5">

                                <Video />

                            </div>

                        </div>

                        <div className="max-w bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">

                            <div className="p-5">

                                {/* <PdfRender /> */}

                            </div>

                        </div>

                        <div className="max-w bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">

                            <div className="p-5">

                                {/* <Pdf2 /> */}
                                
                            </div>

                            <hr />

                            <div className="p-5">

                                {/* <AudioSpotify /> */}

                            </div>

                        </div>

                    </div>

                </div>
                
            </div>
            <PdfRender />
            
        </section>

    )

}

